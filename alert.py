import subprocess
import time


def display_warning(battery_state):
            subprocess.call(['xmessage', f'Battery low: {battery_state}%'])

def get_battery_state():
    # probably wrongly assuming output is always in:
    #     Battery 0: Charging, 92%, 00:25:27 until charged
    # format
    acpi_output = subprocess.check_output(['acpi']).decode('utf-8')
    return int(''.join(filter(str.isdigit, acpi_output.split()[3])))


if __name__ == '__main__':
    last = get_battery_state() - 1
    alerted_recently = False
    threshold = 20
    while True:
        curr = get_battery_state()
        if curr < last and curr < threshold:
            if not alerted_recently:
                display_warning(curr)
                # wait before next check
                time.sleep(60)
            else:
                # if user has been warned, sleep a bit longer
                time.sleep(300)
            alerted_recently = not alerted_recently
        else:
            time.sleep(60)
        last = curr
